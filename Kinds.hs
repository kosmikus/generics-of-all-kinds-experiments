{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoStarIsType #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE QuantifiedConstraints #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -Wall -Wno-unticked-promoted-constructors #-}
module Kinds where

import Data.Kind
import Data.Proxy
import Data.Type.Equality
import qualified GHC.Generics as GHC

--
-- * Administrative / type-level preliminaries
--

type Kind = Type

data Env (k :: [Kind]) where
  Eps   :: Env '[]
  (:&:) :: k -> Env ks -> Env (k : ks)
infixr 5 :&:

data SList (xs :: [k]) where
  SNil :: SList '[]
  SCons :: SList xs -> SList (x : xs)

data SEnv (env :: Env ks) where
  SEps :: SEnv Eps
  SAnd :: SEnv env -> SEnv (k :&: env)

type family Arr (ks :: [Kind]) :: Kind where
  Arr '[]      = Type
  Arr (k : ks) = k -> Arr ks

data (f :: Arr ks) :@@: (x :: Env ks) where
  A0  :: f -> f :@@: Eps
  Arg :: f t :@@: ts -> f :@@: (t :&: ts)

a1 :: f t -> f :@@: (t :&: Eps)
a1 x = Arg (A0 x)

unA1 :: f :@@: (t :&: Eps) -> f t
unA1 (Arg (A0 x)) = x

data Elem (k :: Kind) (d :: [Kind]) where
  Here  :: Elem x (x : xs)
  There :: Elem y xs -> Elem y (x : xs)

--
-- * Generic class and universe
--

type RepType sig = Env sig -> Type

class Generic (f :: Arr sig) (x :: Env sig) where
  type Rep f :: RepType sig

  from :: f :@@: x -> Rep f x
  to :: Rep f x -> f :@@: x

data Unit :: RepType sig where
  Unit :: Unit sig

data (:*:) :: RepType sig -> RepType sig -> RepType sig where
  (:*:) :: f env -> g env -> (f :*: g) env

data (:+:) :: RepType sig -> RepType sig -> RepType sig where
  Inl :: f env -> (f :+: g) env
  Inr :: g env -> (f :+: g) env

newtype Par (t :: Atom sig Type) :: RepType sig where
  Par :: { unPar :: RunAtom t env } -> Par t env

data (:=>:) (t :: Atom sig Constraint) (f :: RepType sig) :: RepType sig where
  Constr :: RunAtom t env => f env -> (t :=>: f) env

data Ex (f :: RepType (x : sig)) :: RepType sig where
  Ex :: f (a :&: env) -> Ex f env

data Atom (sig :: [Kind]) (k :: Kind) where
  Var :: Elem k sig -> Atom sig k
  TyCon :: k -> Atom sig k
  (:@:) :: Atom sig (k1 -> k2) -> Atom sig k1 -> Atom sig k2

type family RunAtom (t :: Atom sig k) (env :: Env sig) :: k where
  RunAtom (Var Here)      (t :&: ts) = t
  RunAtom (Var (There i)) (t :&: ts) = RunAtom (Var i) ts
  RunAtom (TyCon t)       env        = t
  RunAtom (f :@: x)       env        = (RunAtom f env) (RunAtom x env)

--
-- * Example instances of Generic class
--

instance Generic Either (a :&: b :&: Eps) where
  type Rep Either =
    Par (Var Here) :+: Par (Var (There Here))

  from (Arg (Arg (A0 (Left x))))  = Inl (Par x)
  from (Arg (Arg (A0 (Right y)))) = Inr (Par y)

  to (Inl (Par x)) = Arg (Arg (A0 (Left x)))
  to (Inr (Par y)) = Arg (Arg (A0 (Right y)))

-- Every type has multiple possible instances,
-- which are in principle auto-convertible into
-- each other

instance Generic (Either a) (b :&: Eps) where
  type Rep (Either a) =
    Par (TyCon a) :+: Par (Var Here)

  from (Arg (A0 (Left x)))  = Inl (Par x)
  from (Arg (A0 (Right y))) = Inr (Par y)

  to (Inl (Par x)) = Arg (A0 (Left x))
  to (Inr (Par y)) = Arg (A0 (Right y))

instance Generic (Either a b) Eps where
  type Rep (Either a b) =
    Par (TyCon a) :+: Par (TyCon b)

  from (A0 (Left x))  = Inl (Par x)
  from (A0 (Right y)) = Inr (Par y)

  to (Inl (Par x)) = A0 (Left x)
  to (Inr (Par y)) = A0 (Right y)

instance Generic Bool Eps where
  type Rep Bool =
    Unit :+: Unit

  from (A0 False) = Inl Unit
  from (A0 True) = Inr Unit

  to (Inl Unit) = A0 False
  to (Inr Unit) = A0 True

instance Generic [] (a :&: Eps) where
  type Rep [] =
        Unit
    :+: (Par (Var Here) :*: Par (TyCon [] :@: Var Here))

  from (Arg (A0 [])) = Inl Unit
  from (Arg (A0 (x : xs))) = Inr (Par x :*: Par xs)

  to (Inl Unit) = Arg (A0 [])
  to (Inr (Par x :*: Par xs)) = Arg (A0 (x : xs))

data Perfect a = Z a | S (Perfect (a, a))

instance Generic Perfect (a :&: Eps) where
  type Rep Perfect =
        Par (Var Here)
    :+: Par (TyCon Perfect :@: (TyCon (,) :@: Var Here :@: Var Here))

  from (Arg (A0 (Z x))) = Inl (Par x)
  from (Arg (A0 (S t))) = Inr (Par t)

  to (Inl (Par x)) = Arg (A0 (Z x))
  to (Inr (Par t)) = Arg (A0 (S t))

data Nat = Zero | Suc Nat

data Vec (n :: Nat) (a :: Type) where
  VNil  :: Vec Zero a
  VCons :: a -> Vec n a -> Vec (Suc n) a

instance Generic Vec (n :&: a :&: Eps) where
  type Rep Vec =
        ((TyCon ((~) Zero) :@: Var Here) :=>: Unit)
    :+: (Ex
          ((TyCon (~) :@: (TyCon Suc :@: Var Here) :@: Var (There Here))
          :=>:
            (   Par (Var (There (There Here)))
            :*: Par (TyCon Vec :@: Var Here :@: Var (There (There Here)))
            )
          )
        )

  from (Arg (Arg (A0 VNil))) = Inl (Constr Unit)
  from (Arg (Arg (A0 (VCons x xs)))) = Inr (Ex (Constr (Par x :*: Par xs)))

  to (Inl (Constr Unit)) = Arg (Arg (A0 VNil))
  to (Inr (Ex (Constr (Par x :*: Par xs)))) = Arg (Arg (A0 (VCons x xs)))

data NP (f :: k -> Type) (xs :: [k]) where
  Nil :: NP f '[]
  (:*) :: f x -> NP f xs -> NP f (x : xs)
infixr 5 :*

instance Generic NP (f :&: xs :&: Eps) where
  type Rep NP =
        ((TyCon ((~) '[]) :@: Var (There Here)) :=>: Unit)
    :+: (Ex (Ex
          ((TyCon (~)
             :@: (TyCon '(:) :@: Var Here :@: Var (There Here))
             :@: Var (There (There (There Here))))
          :=>:
            (   Par (Var (There (There Here)) :@: Var Here)
            :*: Par (TyCon NP :@: Var (There (There Here)) :@: Var (There Here))
            )
          )
        ))

  from (Arg (Arg (A0 Nil))) = Inl (Constr Unit)
  from (Arg (Arg (A0 (x :* xs)))) = Inr (Ex (Ex (Constr (Par x :*: Par xs))))

  to (Inl (Constr Unit)) = Arg (Arg (A0 Nil))
  to (Inr (Ex (Ex (Constr (Par x :*: Par xs))))) = Arg (Arg (A0 (x :* xs)))

-- Dependent kind example

data F b k (a :: k) = MkF k (Proxy a)
  deriving GHC.Generic

-- Here, we can currently only provide a partially applied instance

instance Generic (F b k) (a :&: Eps) where
  type Rep (F b k) =
    Par (TyCon k) :*: Par (TyCon Proxy :@: Var Here)

  from (Arg (A0 (MkF x y))) = Par x :*: Par y
  to (Par x :*: Par y) = Arg (A0 (MkF x y))

-- singleton natural numbers

data SNat (n :: Nat) where
  SZero :: SNat Zero
  SSuc  :: SNat n -> SNat (Suc n)

instance Generic SNat (n :&: Eps) where
  type Rep SNat =
        ((TyCon ((~) Zero) :@: Var Here) :=>: Unit)
    :+: (Ex
          ((TyCon (~)
             :@: (TyCon Suc :@: Var Here)
             :@: Var (There Here))
          :=>:
            (Par (TyCon SNat :@: Var Here))
          )
        )

  from (Arg (A0 SZero)) = Inl (Constr Unit)
  from (Arg (A0 (SSuc n))) = Inr (Ex (Constr (Par n)))

  to (Inl (Constr Unit)) = Arg (A0 SZero)
  to (Inr (Ex (Constr (Par n)))) = Arg (A0 (SSuc n))

-- SList from above

instance Generic SList (xs :&: Eps) where
  type Rep SList =
        ((TyCon ((~) '[]) :@: Var Here) :=>: Unit)
    :+: (Ex (Ex
          ((TyCon (~)
             :@: (TyCon '(:) :@: Var Here :@: Var (There Here))
             :@: Var (There (There Here)))
          :=>:
            (Par (TyCon SList :@: Var (There Here)))
          )
        ))

  from (Arg (A0 SNil)) = Inl (Constr Unit)
  from (Arg (A0 (SCons xs))) = Inr (Ex (Ex (Constr (Par xs))))

  to (Inl (Constr Unit)) = Arg (A0 SNil)
  to (Inr (Ex (Ex (Constr (Par xs))))) = Arg (A0 (SCons xs))

-- De-Bruijn lambda terms

--
-- * Example function definitions
--

-- Generic equality

class GEq (f :: RepType ks) (env1 :: Env ks) (env2 :: Env ks) where
  geq :: f env1 -> f env2 -> Bool

instance GEq Unit env1 env2 where
  geq Unit Unit = True

instance (GEq a env1 env2, GEq b env1 env2) => GEq (a :+: b) env1 env2 where
  geq (Inl x1) (Inl x2) = geq x1 x2
  geq (Inr y1) (Inr y2) = geq y1 y2
  geq _ _ = False

instance (GEq a env1 env2, GEq b env1 env2) => GEq (a :*: b) env1 env2 where
  geq (x1 :*: y1) (x2 :*: y2) = geq x1 x2 && geq y1 y2

instance (env1 ~ env2, Eq (RunAtom t env1)) => GEq (Par t) env1 env2 where
  geq (Par x1) (Par x2) = x1 == x2

instance ((RunAtom c env1, RunAtom c env2) => GEq a env1 env2) => GEq (c :=>: a) env1 env2 where
  geq (Constr x1) (Constr x2) = geq x1 x2

instance (forall x1 x2 . GEq a (x1 :&: env1) (x2 :&: env2)) => GEq (Ex a) env1 env2 where
  geq (Ex x1) (Ex x2) = geq x1 x2

-- TestEquality

data Equalities (env1 :: Env sig) (env2 :: Env sig) :: Type where
  EEmpty :: Equalities env1 env2
  EAll   :: Equalities env env
  ECons  :: Maybe (x1 :~: x2) -> Equalities env1 env2 -> Equalities (x1 :&: env1) (x2 :&: env2)

zipEqualities :: Equalities env1 env2 -> Equalities env1 env2 -> Equalities env1 env2
zipEqualities EAll                     _                = EAll
zipEqualities _                        EAll             = EAll
zipEqualities (ECons (Just Refl) eqs1) (ECons _   eqs2) = ECons (Just Refl) (zipEqualities eqs1 eqs2)
zipEqualities (ECons Nothing     eqs1) (ECons eq2 eqs2) = ECons eq2         (zipEqualities eqs1 eqs2)
zipEqualities EEmpty                   eqs2             = eqs2
zipEqualities eqs1                     EEmpty           = eqs1

class GTestEquality (f :: RepType sig) (env1 :: Env sig) (env2 :: Env sig) where
  gtestEquality :: f env1 -> f env2 -> Equalities env1 env2

instance (env1 ~ env2) => GTestEquality Unit env1 env2 where
  gtestEquality _ _ = EAll

instance (GTestEquality a env1 env2, GTestEquality b env1 env2) => GTestEquality (a :+: b) env1 env2 where
  gtestEquality (Inl x1) (Inl x2) = gtestEquality x1 x2
  gtestEquality (Inr y1) (Inr y2) = gtestEquality y1 y2
  gtestEquality _ _ = EEmpty

instance (GTestEquality a env1 env2, GTestEquality b env1 env2) => GTestEquality (a :*: b) env1 env2 where
  gtestEquality (x1 :*: y1) (x2 :*: y2) =
    zipEqualities (gtestEquality x1 x2) (gtestEquality y1 y2)

instance (env1 ~ env2) => GTestEquality (Par (TyCon a)) env1 env2 where
  gtestEquality _ _ = EAll

instance (TestEquality f) => GTestEquality (Par (TyCon f :@: Var Here)) (x1 :&: env1) (x2 :&: env2) where
  gtestEquality (Par x1) (Par x2) =
    case testEquality x1 x2 of
      Just Refl -> ECons (Just Refl) EEmpty
      Nothing   -> EEmpty

instance (TestEquality f, GTestEquality (Par (TyCon f :@: Var i)) env1 env2) => GTestEquality (Par (TyCon f :@: Var (There i))) (x1 :&: env1) (x2 :&: env2) where
  gtestEquality (Par x1) (Par x2) =
    ECons Nothing (gtestEquality @_ @(Par (TyCon f :@: Var i)) (Par x1) (Par x2))

instance ((RunAtom c env1, RunAtom c env2) => GTestEquality a env1 env2) => GTestEquality (c :=>: a) env1 env2 where
  gtestEquality (Constr x1) (Constr x2) = gtestEquality x1 x2


{-
class (GTestEquality f (x1 :&: env1) (x2 :&: env2), x1 ~ x2 => env1 ~ env2) => SupGTestEquality (f :: RepType (k : sig)) (x1 :: k) (x2 :: k) (env1 :: Env sig) (env2 :: Env sig)
instance (GTestEquality f (x1 :&: env1) (x2 :&: env2), x1 ~ x2 => env1 ~ env2) => SupGTestEquality (f :: RepType (k : sig)) (x1 :: k) (x2 :: k) (env1 :: Env sig) (env2 :: Env sig)
-}

instance (forall x1 x2 . (RunAtom c (x1 :&: env1), RunAtom c (x2 :&: env2)) => (GTestEquality a (x1 :&: env1) (x2 :&: env2))
         ,forall x1 x2 . (RunAtom c (x1 :&: env1), RunAtom c (x2 :&: env2), x1 ~ x2) => (x1 :&: env1) ~ (x2 :&: env2))
         => GTestEquality (Ex (c :=>: a)) env1 env2 where
  gtestEquality (Ex (Constr x1)) (Ex (Constr x2)) =
    case liftEqualities (gtestEquality x1 x2) of
      EAll -> EAll
      ECons _ eqs -> eqs
      EEmpty -> EEmpty

instance (forall x1 x2 y1 y2 . (RunAtom c (x1 :&: y1 :&: env1), RunAtom c (x2 :&: y2 :&: env2)) => (GTestEquality a (x1 :&: y1 :&: env1) (x2 :&: y2 :&: env2))
         ,forall x1 x2 y1 y2 . (RunAtom c (x1 :&: y1 :&: env1), RunAtom c (x2 :&: y2 :&: env2), x1 ~ x2, y1 ~ y2) => (x1 :&: y1 :&: env1) ~ (x2 :&: y2 :&: env2))
         => GTestEquality (Ex (Ex (c :=>: a))) env1 env2 where
  gtestEquality (Ex (Ex (Constr x1))) (Ex (Ex (Constr x2))) =
    case liftEqualities2 (gtestEquality x1 x2) of
      EAll -> EAll
      ECons _ (ECons _ eqs) -> eqs
      ECons _ EAll -> EAll
      ECons _ EEmpty -> EEmpty
      EEmpty -> EEmpty

liftEqualities2 ::
  forall (x1 :: k) (x2 :: k) (y1 :: l) (y2 :: l) (env1 :: Env sig) (env2 :: Env sig) .
  ((x1 ~ x2, y1 ~ y2) => (x1 :&: y1 :&: env1) ~ (x2 :&: y2 :&: env2))
  => Equalities (x1 :&: y1 :&: env1) (x2 :&: y2 :&: env2) -> Equalities (x1 :&: y1 :&: env1) (x2 :&: y2 :&: env2)
liftEqualities2 (ECons (Just Refl) (ECons (Just Refl) _)) = ECons (Just Refl) (ECons (Just Refl) (withEq2 @x1 @x2 @y1 @y2 @env1 @env2 EAll))
liftEqualities2 x = x

liftEqualities ::
  forall (x1 :: k) (x2 :: k) (env1 :: Env sig) (env2 :: Env sig) .
  ((x1 ~ x2) => (x1 :&: env1) ~ (x2 :&: env2))
  => Equalities (x1 :&: env1) (x2 :&: env2) -> Equalities (x1 :&: env1) (x2 :&: env2)
liftEqualities (ECons (Just Refl) _) = ECons (Just Refl) (withEq @x1 @x2 @env1 @env2 EAll)
liftEqualities x = x

withEq :: forall x1 x2 env1 env2 x . ((env1 ~ env2) => x) -> ((x1 :&: env1) ~ (x2 :&: env2) => x)
withEq k = k

withEq2 :: forall x1 x2 y1 y2 env1 env2 x . ((env1 ~ env2) => x) -> ((x1 :&: y1 :&: env1) ~ (x2 :&: y2 :&: env2) => x)
withEq2 k = k

{-
instance (forall x1 x2 . GTestEquality a (x1 :&: env1) (x2 :&: env2)) => GTestEquality (Ex a) env1 env2 where
  gtestEquality (Ex x1) (Ex x2) =
    case gtestEquality x1 x2 of
      EAll                  -> EAll
      ECons (Just Refl) eqs -> eqs
      _                     -> EEmpty
-}


-- Minimal effort to define fmap on types of kind Type -> Type

class SimpleGFunctor (f :: RepType '[Type]) where
  sfmap :: (a -> b) -> f (a :&: Eps) -> f (b :&: Eps)

instance SimpleGFunctor Unit where
  sfmap _ Unit = Unit

instance (SimpleGFunctor f, SimpleGFunctor g) => SimpleGFunctor (f :+: g) where
  sfmap mappings (Inl x) = Inl (sfmap mappings x)
  sfmap mappings (Inr y) = Inr (sfmap mappings y)

instance (SimpleGFunctor f, SimpleGFunctor g) => SimpleGFunctor (f :*: g) where
  sfmap mappings (x :*: y) = sfmap mappings x :*: sfmap mappings y

instance SimpleGFunctor (Par (Var Here)) where
  sfmap f (Par x) = Par (f x)

instance SimpleGFunctor (Par (TyCon a)) where
  sfmap _ (Par x) = Par x

instance (Functor f, SimpleGFunctor (Par t)) => SimpleGFunctor (Par (TyCon f :@: t)) where
  sfmap g (Par fx) = Par (fmap (\ a -> unPar (sfmap @(Par t) g (Par a))) fx)

listMap :: (a -> b) -> [a] -> [b]
listMap f xs = unA1 (to (sfmap f (from (a1 xs))))

-- More general fmap function with fewer restrictions

class GFunctor (f :: RepType ks) (env1 :: Env ks) (env2 :: Env ks) where
  gfmap :: Mappings env1 env2 -> f env1 -> f env2

data Mappings (env1 :: Env ks) (env2 :: Env ks) :: Type where
  None     :: Mappings Eps Eps
  (:&&:)   :: MapType x1 x2 -> Mappings env1 env2 -> Mappings ((x1 :: k) :&: env1) ((x2 :: k) :&: env2)

data family MapType (x1 :: k) (x2 :: k)
data instance MapType x1 x2 = MapStar (x1 -> x2)
data instance MapType (f1 :: k1 -> k2) (f2 :: k1 -> k2) =
  MapArrow (forall a1 a2 . MapType a1 a2 -> MapType (f1 a1) (f2 a2))

instance GFunctor Unit env1 env2 where
  gfmap _ Unit = Unit

instance (GFunctor f env1 env2, GFunctor g env1 env2) => GFunctor (f :+: g) env1 env2 where
  gfmap mappings (Inl x) = Inl (gfmap mappings x)
  gfmap mappings (Inr y) = Inr (gfmap mappings y)

instance (GFunctor f env1 env2, GFunctor g env1 env2) => GFunctor (f :*: g) env1 env2 where
  gfmap mappings (x :*: y) = gfmap mappings x :*: gfmap mappings y

instance (GFunctorPar t env1 env2) => GFunctor (Par t) env1 env2 where
  gfmap mappings (Par x) = Par $
    case gfmapPar @_ @_ @t mappings of
      MapStar f -> f x

class KFunctor (a :: k) where
  kmap :: MapType a a

instance KFunctor (a :: Type) where
  kmap = MapStar id

instance (forall a . Generic f (a :&: Eps), rf ~ Rep f, forall a b . GFunctor rf (a :&: Eps) (b :&: Eps)) => KFunctor (f :: Type -> Type) where
  kmap =
    MapArrow
      (\ f -> MapStar (\ fx ->
        case (to (gfmap (f :&&: None) (from (Arg (A0 fx))))) of
          Arg (A0 gx) -> gx))

class GFunctorPar (t :: Atom sig k) (env1 :: Env sig) (env2 :: Env sig) where
  gfmapPar :: Mappings env1 env2 -> MapType (RunAtom t env1) (RunAtom t env2)

instance KFunctor a => GFunctorPar (TyCon a :: Atom sig k) env1 env2 where
  gfmapPar _ = kmap

instance (GFunctorPar f env1 env2, GFunctorPar a env1 env2) => GFunctorPar (f :@: a) env1 env2 where
  gfmapPar env =
    case gfmapPar @_ @_ @f env of
      MapArrow g -> g (gfmapPar @_ @_ @a env)

instance GFunctorPar (Var Here :: Atom (k : sig) k) (x1 :&: env1) (x2 :&: env2) where
  gfmapPar (f :&&: _) = f

instance GFunctorPar (Var i) env1 env2 => GFunctorPar (Var (There i) :: Atom (Type : sig) Type) (x1 :&: env1) (x2 :&: env2) where
  gfmapPar (_ :&&: fs) = gfmapPar @_ @_ @(Var i) fs

instance ((RunAtom c env1) => GFunctor a env1 env2, RunAtom c env2) => GFunctor (c :=>: a) env1 env2 where
  gfmap mappings (Constr x) = Constr (gfmap mappings x)

instance (forall (x :: Type) . GFunctor f (x :&: env1) (x :&: env2)) => GFunctor (Ex f) env1 env2 where
  gfmap mappings (Ex x1) = Ex (gfmap (MapStar id :&&: mappings) x1)

newtype FromGeneric0 f     = FromGeneric0  f
newtype FromGeneric1 f a   = FromGeneric1 (f a)
newtype FromGeneric2 f a b = FromGeneric2 (f a b)

instance (Generic f Eps, GEq (Rep f) Eps Eps) => Eq (FromGeneric0 f) where
  FromGeneric0 x1 == FromGeneric0 x2 =
    geq (from (A0 x1)) (from (A0 x2))

instance (Generic f (a :&: Eps), GEq (Rep f) (a :&: Eps) (a :&: Eps)) => Eq (FromGeneric1 f a) where
  FromGeneric1 x1 == FromGeneric1 x2 =
    geq (from (Arg (A0 x1))) (from (Arg (A0 x2)))

instance (Generic f (a :&: b :&: Eps), GEq (Rep f) (a :&: b :&: Eps) (a :&: b :&: Eps)) => Eq (FromGeneric2 f a b) where
  FromGeneric2 x1 == FromGeneric2 x2 =
    geq (from (Arg (Arg (A0 x1)))) (from (Arg (Arg (A0 x2))))

instance ((forall a . Generic f (a :&: Eps)), rf ~ Rep f, (forall a b . GTestEquality rf (a :&: Eps) (b :&: Eps))) => TestEquality (FromGeneric1 f) where
  testEquality (FromGeneric1 x1) (FromGeneric1 x2) =
    case gtestEquality (from (Arg (A0 x1))) (from (Arg (A0 x2))) of
      EAll                     -> Just Refl
      ECons (Just Refl) EEmpty -> Just Refl
      _ -> Nothing

-- deriving via FromGeneric0 Foo instance Eq Foo -- doesn't work, and shouldn't
deriving via FromGeneric1 Perfect a instance Eq a => Eq (Perfect a)
deriving via FromGeneric2 Vec n a instance Eq a => Eq (Vec n a)
deriving via FromGeneric2 NP f xs instance AllF Eq f xs => Eq (NP f xs)

data Foo where
  MkFoo :: a -> Foo

instance Generic Foo Eps where
  type Rep Foo = Ex (Par (Var Here))

  from (A0 (MkFoo a)) = Ex (Par a)
  to (Ex (Par a)) = A0 (MkFoo a)


-- deriving instance AllF Eq f xs => Eq (NP f xs)

type family AllF (c :: Type -> Constraint) (f :: k -> Type) (xs :: [k]) :: Constraint where
  AllF c f '[] = ()
  AllF c f (x : xs) = (c (f x), AllF c f xs)


data MyGadt a where
  G1 :: Int -> MyGadt Int
  G2 :: Bool -> MyGadt String

type family Any :: k

instance Generic MyGadt (a :&: Eps) where
  type Rep MyGadt =
        ((TyCon ((~) Int) :@: Var Here) :=>: Par (TyCon Int))
    :+: ((TyCon ((~) String) :@: Var Here) :=>: Par (TyCon Bool))

  from (Arg (A0 (G1 n))) = Inl (Constr (Par n))
  from (Arg (A0 (G2 b))) = Inr (Constr (Par b))

  to (Inl (Constr (Par n))) = Arg (A0 (G1 n))
  to (Inr (Constr (Par b))) = Arg (A0 (G2 b))

deriving via FromGeneric1 MyGadt instance TestEquality MyGadt

-- deriving via FromGeneric1 SList instance TestEquality SList

deriving via FromGeneric1 SNat instance TestEquality SNat

{-
instance TestEquality MyGadt where
  testEquality (G1 _) (G1 _) = Just Refl
  testEquality (G2 _) (G2 _) = Just Refl
  testEquality _ _ = Nothing
-}

deriving instance Eq (MyGadt a)

data SomeMyGadt where
  SomeMyGadt :: MyGadt a -> SomeMyGadt

instance Generic SomeMyGadt Eps where
  type Rep SomeMyGadt =
    Ex (   ((TyCon ((~) Int) :@: Var Here) :=>: Par (TyCon Int))
       :+: ((TyCon ((~) String) :@: Var Here) :=>: Par (TyCon Bool))
       )

  from (A0 (SomeMyGadt (G1 n))) = Ex (Inl (Constr (Par n)))
  from (A0 (SomeMyGadt (G2 b))) = Ex (Inr (Constr (Par b)))

  to (Ex (Inl (Constr (Par n)))) = A0 (SomeMyGadt (G1 n))
  to (Ex (Inr (Constr (Par b)))) = A0 (SomeMyGadt (G2 b))

deriving via FromGeneric0 SomeMyGadt instance Eq SomeMyGadt

{-
instance Eq SomeMyGadt where
  (SomeMyGadt (G1 n1)) == (SomeMyGadt (G1 n2)) = n1 == n2
  (SomeMyGadt (G2 b1)) == (SomeMyGadt (G2 b2)) = b1 == b2
  _ == _ = False
-}

